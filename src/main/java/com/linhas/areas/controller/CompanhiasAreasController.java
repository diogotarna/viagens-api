package com.linhas.areas.controller;

import com.linhas.areas.entity.Companhia;
import com.linhas.areas.repository.AeroportosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/api/companhias")
public class CompanhiasAreasController {

	@Autowired
	AeroportosRepository aeroportoRepository;

	@GetMapping
	public ResponseEntity<Iterable<Companhia>> consultarTodos() {
		Iterable<Companhia> aeroporto  = aeroportoRepository.findAll();
		return aeroporto != null ? ResponseEntity.ok(aeroporto) : ResponseEntity.notFound().build();

	}

}

package com.linhas.areas.controller;

import com.linhas.areas.entity.RegrasDeNegocio;
import com.linhas.areas.dto.LinhasAereaDTO;
import com.linhas.areas.service.LinhasAereaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/api/linhas")
public class LinhasAreasController {

	@Autowired
	LinhasAereaService linhasAereasService;

	@PostMapping(value = "/consultar")
	public ResponseEntity<Iterable<LinhasAereaDTO>> consultarVoos(@RequestBody RegrasDeNegocio regrasDeNegocio) {
		Iterable<LinhasAereaDTO> retorno = linhasAereasService.consultarLinhas(regrasDeNegocio);
		return retorno != null ? ResponseEntity.ok(retorno) : ResponseEntity.noContent().build();
	}

}

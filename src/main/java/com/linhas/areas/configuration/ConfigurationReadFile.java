package com.linhas.areas.configuration;

import com.google.common.io.Resources;
import com.linhas.areas.rules.FuncaoUtils;
import com.linhas.areas.dto.AeroportosDTO;
import com.linhas.areas.dto.PlanesDTO;
import com.linhas.areas.dto.UberairDTO;
import com.linhas.areas.entity.Companhia;
import com.linhas.areas.entity.LinhasAerea;
import com.linhas.areas.repository.AeroportosRepository;
import com.linhas.areas.repository.LinhasAereaRepository;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import javax.annotation.PostConstruct;
import java.io.*;
import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

@Configuration
public class ConfigurationReadFile {

    private static final Logger LOGGER = Logger.getLogger(ConfigurationReadFile.class.getName());

    @Autowired
    AeroportosRepository aeroportoRepository;

    @Autowired
    LinhasAereaRepository planesRepository;


    @PostConstruct
    public void afterPropertiesSet() throws Exception {

        String aux = ReaderResourceJsonStatic("aeroportos.json");

        Gson gson = new Gson();
        Type collectionType = new TypeToken<List<AeroportosDTO>>() {
        }.getType();
        List<AeroportosDTO> listAeroportoDTO = gson.fromJson(aux, collectionType);
        List<Companhia> listaAeroporto = new LinkedList<Companhia>();
        for (AeroportosDTO dto : listAeroportoDTO) {
            Companhia aeroporto = new Companhia(dto);
            listaAeroporto.add(aeroporto);
        }
        FuncaoUtils.setListaCompanhias(listaAeroporto);

        LOGGER.info("Lista de Companhias Aereas carregadas.");

        aux = ReaderResourceJsonStatic("99planes.json");
        List<LinhasAerea> listaLinhasAereas = new LinkedList<LinhasAerea>();

        gson = new Gson();
        collectionType = new TypeToken<List<PlanesDTO>>() {
        }.getType();
        List<PlanesDTO> list99PlanesDTO = gson.fromJson(aux, collectionType);

        for (PlanesDTO dto : list99PlanesDTO) {
            LinhasAerea linhasAerea = new LinhasAerea(dto);
            listaLinhasAereas.add(linhasAerea);
        }

        List<String[]> uberairs = ReaderResourceCSVStatic("uberair.csv");
        for (String[] uberair : uberairs) {

            UberairDTO uberairDTO = new UberairDTO(uberair);
            LinhasAerea linhasAerea = new LinhasAerea(uberairDTO);
            listaLinhasAereas.add(linhasAerea);

        }
        FuncaoUtils.setListaLinhasAreas(listaLinhasAereas);

        LOGGER.info("Lista de Linhas Aereas carregadas.");
    }


    public String ReaderResourceJsonStatic(String text) throws Exception {
        String aux = "";

        InputStream inputStream = Resources.getResource(text).openStream();
        InputStreamReader isr = new InputStreamReader(inputStream,
                StandardCharsets.UTF_8);

        BufferedReader br = new BufferedReader(isr);

        while (br.ready()) {
            String auxReader = br.readLine();
            aux += auxReader;
        }
        br.close();
        return aux;
    }


    public List<String[]> ReaderResourceCSVStatic(String text) throws Exception {

        InputStream inputStream = Resources.getResource(text).openStream();
        InputStreamReader isr = new InputStreamReader(inputStream,
                StandardCharsets.UTF_8);

        Reader reader = isr;
        CSVReader csvReader = new CSVReaderBuilder(reader).withSkipLines(1).build();
        List<String[]> retorno = csvReader.readAll();
        reader.close();
        return retorno;
    }

}
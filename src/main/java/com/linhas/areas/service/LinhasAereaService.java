package com.linhas.areas.service;

import java.util.*;
import java.util.stream.Collectors;

import com.linhas.areas.rules.RegrasUtils;
import com.linhas.areas.entity.RegrasDeNegocio;
import com.linhas.areas.dto.LinhasAereaDTO;
import com.linhas.areas.entity.LinhasAerea;
import com.linhas.areas.repository.LinhasAereaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class LinhasAereaService {

    @Autowired
    LinhasAereaRepository linhasAereaRepository;

    public List<LinhasAereaDTO> consultarLinhas(RegrasDeNegocio regras) {

        List<LinhasAerea> todas = linhasAereaRepository.findAll();
        List<LinhasAerea> filtro = filtrar(todas,regras);
        List<LinhasAereaDTO> trechos = new LinkedList<>();

        trechos.addAll(linhasConexao(regras, filtro));
        trechos.addAll(linhasDiretas(regras, todas));

        RegrasUtils.compararListaDTO(trechos);

        return trechos;
    }

    public List<LinhasAerea> filtrar (List<LinhasAerea> lista, RegrasDeNegocio regras){
        List<LinhasAerea> retorno = new LinkedList<>();
        LinhasAerea inicio = new LinhasAerea();

        for (LinhasAerea linha : lista){
            if(linha.getOrigem().equals(regras.getOrigem())){
                retorno.add(linha);
            }

            if(linha.getDestino().equals(regras.getDestino())){
                retorno.add(linha);
            }

            if((linha.getDestino().equals(inicio.getOrigem())) && (linha.getDestino().equals(regras.getOrigem())) &&
                    ((linha.getOrigem().equals(regras.getDestino())))){
                retorno.add(linha);
            }
        }

        return retorno;
    }


    private Collection<? extends LinhasAereaDTO> linhasDiretas(RegrasDeNegocio regras, List<LinhasAerea> lista) {
        List<LinhasAerea> filtrada = lista.stream().filter(regra -> RegrasUtils.verificarConexao(regra, regras))
                .collect(Collectors.toList());
        return conexaoDireta(filtrada);
    }

    private Collection<? extends LinhasAereaDTO> linhasConexao(RegrasDeNegocio regras, List<LinhasAerea> lista) {

        List<LinhasAereaDTO> retorno = new LinkedList<>();

        List<LinhasAerea> escalaInicio = RegrasUtils.compararListaAplicaRegraSaida(lista, regras);

        List<LinhasAerea> escalaFim = RegrasUtils.compararListaAplicaRegraChegada(lista, regras);

        for (LinhasAerea inicio : escalaInicio) {

            List<LinhasAerea> conexao = new LinkedList<>();

            conexao.add(inicio);

            for (LinhasAerea fim : escalaFim) {

                if (RegrasUtils.verificar(inicio, fim, regras)) {

                    if(!conexao.contains(fim) && (conexao.size()<=2)) {
                        if (conexao.size()>1){
                            if(RegrasUtils.verificarConexaoExistente(fim,conexao.get(1))){
                                conexao.add(fim);
                            }
                        } else {
                            conexao.add(fim);
                        }
                    }

                    LinhasAereaDTO linhasAereaDTO = new LinhasAereaDTO();

                    linhasAereaDTO.setOrigem(conexao.get(0).getOrigem());
                    linhasAereaDTO.setDestino(fim.getDestino());
                    linhasAereaDTO.setSaida(conexao.get(0).getSaida());
                    linhasAereaDTO.setChegada(fim.getChegada());
                    linhasAereaDTO.setTrechos(conexao);

                    retorno.add(linhasAereaDTO);
                }
            }


        }

        return RegrasUtils.compararListaDTO(retorno);
    }

	public static List<LinhasAereaDTO> conexaoDireta(List<LinhasAerea> linhaAereas) {

		List<LinhasAereaDTO> linhaAereaDTO = new LinkedList<>();

		for (LinhasAerea linhaAerea : linhaAereas) {

			List<LinhasAerea> trechos = new LinkedList<>();
			trechos.add(linhaAerea);

			LinhasAereaDTO dto = new LinhasAereaDTO(
					linhaAerea.getOrigem(),
					linhaAerea.getDestino(),
					linhaAerea.getSaida(),
					linhaAerea.getChegada(),
					trechos);
			
			linhaAereaDTO.add(dto);
		}

		return linhaAereaDTO;

	}


}

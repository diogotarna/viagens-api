package com.linhas.areas.rules;

import com.linhas.areas.entity.Companhia;
import com.linhas.areas.entity.LinhasAerea;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;
import java.util.List;

public class FuncaoUtils {

    private static List<Companhia> listaCompanhias = new LinkedList<Companhia>();

    private static List<LinhasAerea> listaLinhasAreas = new LinkedList<LinhasAerea>();

    private static final DateTimeFormatter formatter = DateTimeFormatter.ISO_DATE;

    public static List<Companhia> getListaCompanhias() {
        return listaCompanhias;
    }

    public static void setListaCompanhias(List<Companhia> listaCompanhias) {
        FuncaoUtils.listaCompanhias = listaCompanhias;
    }
    public static List<LinhasAerea> getListaLinhasAreas() {
        return listaLinhasAreas;
    }

    public static void setListaLinhasAreas(List<LinhasAerea> listaLinhasAreas) {
        FuncaoUtils.listaLinhasAreas = listaLinhasAreas;
    }

    public static LocalDate parseData(String data) {
        LocalDate dateTime = LocalDate.parse(data, formatter);
        return dateTime;
    }

    public static LocalDateTime StrToTLocalDateTime(String data, String tempo){
        LocalDateTime dateTime = LocalDateTime.of(LocalDate.parse(data, formatter), LocalTime.parse(tempo));
        return dateTime;
    }


}

package com.linhas.areas.rules;

import com.linhas.areas.dto.LinhasAereaDTO;
import com.linhas.areas.entity.LinhasAerea;
import com.linhas.areas.entity.RegrasDeNegocio;

import java.time.temporal.ChronoUnit;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class RegrasUtils {

    public static List<LinhasAereaDTO> compararListaDTO(List<LinhasAereaDTO> lista) {
        lista.sort(Comparator.comparing(LinhasAereaDTO::getSaida));
        return lista;
    }

    public static  List<LinhasAerea> compararLista(List<LinhasAerea> lista) {
        lista.sort(Comparator.comparing(LinhasAerea::getData));
        return lista;
    }
    public static  List<LinhasAerea> compararListaAplicaRegraChegada(List<LinhasAerea> lista, RegrasDeNegocio regras) {
        return compararLista(lista).stream().filter(regra -> chegada(regras, regra))
                .collect(Collectors.toList());
    }

    public static List<LinhasAerea> compararListaAplicaRegraSaida(List<LinhasAerea> lista, RegrasDeNegocio regras) {
        return  compararLista(lista).stream().filter(regra -> saida(regras, regra))
                .collect(Collectors.toList());
    }
    public static boolean verificar(LinhasAerea inicio, LinhasAerea fim, RegrasDeNegocio regrasDeNegocio) {
        return (( inicio.getDestino().equals(fim.getOrigem()))  && verificarTempo(inicio, fim)
                && naoRetorna(fim, inicio, regrasDeNegocio));
    }

    public static boolean chegada(RegrasDeNegocio regras, LinhasAerea linha) {
        return  !linha.getOrigem().equals(regras.getOrigem())  && linha.getDestino().equals(regras.getDestino());
    }

    public static boolean saida(RegrasDeNegocio regras, LinhasAerea linha) {

        return linha.getOrigem().equals(regras.getOrigem()) && linha.getData().equals(regras.getData())
                && !linha.getDestino().equals(regras.getDestino());
    }

    public static  boolean naoRetorna(LinhasAerea ultima, LinhasAerea atual, RegrasDeNegocio regrasDeNegocio) {
        return (ultima.getOrigem().equals(atual.getDestino())) &&
                atual.getOrigem().equals(regrasDeNegocio.getOrigem()) &&
                ultima.getDestino().equals(regrasDeNegocio.getDestino());
    }

    public static boolean verificarTempo(LinhasAerea destino, LinhasAerea origem) {

        long tempo = destino.getChegada().until(origem.getSaida(), ChronoUnit.HOURS);

        if (tempo > 0 && tempo <= 12 )
        {
            return true;
        } else {
            return false;
        }

    }

    public static boolean verificarConexao(LinhasAerea linha, RegrasDeNegocio regras) {
        return ( linha.getOrigem().equals(regras.getOrigem()))   && ( linha.getDestino().equals(regras.getDestino()))
                && ( linha.getData().equals(regras.getData()));
    }

    public static boolean verificarConexaoExistente(LinhasAerea linha1, LinhasAerea linha2) {
        boolean result =  ((!linha1.getOrigem().equals(linha2.getOrigem())) &&
                   (!linha1.getDestino().equals(linha2.getDestino())));
        return result;
    }
}

package com.linhas.areas.repository;

import com.linhas.areas.rules.FuncaoUtils;
import com.linhas.areas.entity.Companhia;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public class AeroportosRepository {

    public List<Companhia> findAll(){
        return FuncaoUtils.getListaCompanhias();
    };

}

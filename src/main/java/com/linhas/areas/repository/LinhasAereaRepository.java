package com.linhas.areas.repository;

import com.linhas.areas.rules.FuncaoUtils;
import com.linhas.areas.entity.LinhasAerea;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public class LinhasAereaRepository {

    public List<LinhasAerea> findAll(){ return FuncaoUtils.getListaLinhasAreas(); }

}



package com.linhas.areas.dto;

import lombok.Getter;
import lombok.Setter;

import java.lang.reflect.Field;
import java.sql.Time;
import java.util.Date;

public class PlanesDTO {

    @Getter @Setter
    private String voo;
    @Getter @Setter
    private String origem;
    @Getter @Setter
    private String destino;
    @Getter @Setter
    private String data_saida;
    @Getter @Setter
    private String saida;
    @Getter @Setter
    private String chegada;
    @Getter @Setter
    private Double valor;

    public PlanesDTO(String[] planes) {

        this.voo = planes[0];
        this.origem = planes[1];
        this.destino = planes[2];
        this.data_saida = planes[3];
        this.saida = planes[4];
        this.chegada = planes[5];
        this.valor = Double.valueOf(planes[6]);
    }


    @Override
    public String toString() {
        /*
         * Alteração de metodo toString()
         *
         * Diogo Tarnawisky
         *
         */

        StringBuffer sb = new StringBuffer();
        sb.append(getClass().getSimpleName().toUpperCase());
        sb.append(" [ ");
        Class<?> classe = this.getClass();

        Field fields[] = classe.getDeclaredFields();

        int count = 0;

        for (Field field : fields ) {
            count++;

            Object obj;

            try {

                obj = field.get(this);
                if (obj instanceof String) {
                    sb.append(field.getName());
                    sb.append(" = ");
                    sb.append(obj.toString());

                    if(count<fields.length)
                        sb.append(", ");

                }

            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }

        }


        sb.append(" ] ");
        return sb.toString();
    }

}

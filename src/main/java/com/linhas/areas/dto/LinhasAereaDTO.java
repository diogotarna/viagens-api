package com.linhas.areas.dto;

import com.linhas.areas.entity.LinhasAerea;
import lombok.Getter;
import lombok.Setter;

import java.lang.reflect.Field;
import java.time.LocalDateTime;
import java.util.List;

public class LinhasAereaDTO {

	@Getter @Setter
	private String origem;
	@Getter @Setter
	private String destino;
	@Getter @Setter
	private LocalDateTime saida;
	@Getter @Setter
	private LocalDateTime chegada;
	@Getter @Setter
	private List<LinhasAerea> trechos;

	public LinhasAereaDTO() {
		
	}

	public LinhasAereaDTO(String origem, String destino, LocalDateTime saida, LocalDateTime chegada, List<LinhasAerea> trechos) {
		this.origem = origem;
		this.destino = destino;
		this.saida = saida;
		this.chegada = chegada;
		this.trechos = trechos;
	}

	public String getOrigem() {
		return origem;
	}

	public void setOrigem(String origem) {
		this.origem = origem;
	}

	public String getDestino() {
		return destino;
	}

	public void setDestino(String destino) {
		this.destino = destino;
	}

	public LocalDateTime getSaida() {
		return saida;
	}

	public void setSaida(LocalDateTime saida) {
		this.saida = saida;
	}

	public LocalDateTime getChegada() {
		return chegada;
	}

	public void setChegada(LocalDateTime chegada) {
		this.chegada = chegada;
	}

	public List<LinhasAerea> getTrechos() {
		return trechos;
	}

	public void setTrechos(List<LinhasAerea> trechos) {
		this.trechos = trechos;
	}


	@Override
	public String toString() {
		/*
		 * Alteração de metodo toString()
		 *
		 * Diogo Tarnawisky
		 *
		 */

		StringBuffer sb = new StringBuffer();
		sb.append(getClass().getSimpleName().toUpperCase());
		sb.append(" [ ");
		Class<?> classe = this.getClass();

		Field fields[] = classe.getDeclaredFields();

		int count = 0;

		for (Field field : fields ) {
			count++;

			Object obj;

			try {

				obj = field.get(this);
				if (obj instanceof String) {
					sb.append(field.getName());
					sb.append(" = ");
					sb.append(obj.toString());

					if(count<fields.length)
						sb.append(", ");

				}

			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}


		sb.append(" ] ");
		return sb.toString();
	}
}

package com.linhas.areas.dto;

import lombok.Getter;
import lombok.Setter;

import java.lang.reflect.Field;

public class AeroportosDTO {

    @Getter @Setter
    private String nome;
    @Getter @Setter
    private String aeroporto;
    @Getter @Setter
    private String cidade;

    @Override
    public String toString() {
        /*
         * Alteração de metodo toString()
         *
         * Diogo Tarnawisky
         *
         */

        StringBuffer sb = new StringBuffer();
        sb.append(getClass().getSimpleName().toUpperCase());
        sb.append(" [ ");
        Class<?> classe = this.getClass();

        Field fields[] = classe.getDeclaredFields();

        int count = 0;

        for (Field field : fields ) {
            count++;

            Object obj;

            try {

                obj = field.get(this);
                if (obj instanceof String) {
                    sb.append(field.getName());
                    sb.append(" = ");
                    sb.append(obj.toString());

                    if(count<fields.length)
                        sb.append(", ");

                }

            } catch (IllegalArgumentException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }


        sb.append(" ] ");
        return sb.toString();
    }

}

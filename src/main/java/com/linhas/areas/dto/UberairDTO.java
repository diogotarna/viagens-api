package com.linhas.areas.dto;

import lombok.Getter;
import lombok.Setter;

import java.lang.reflect.Field;

public class UberairDTO {

    @Getter @Setter
    private String numero_voo;
    @Getter @Setter
    private String aeroporto_origem;
    @Getter @Setter
    private String aeroporto_destino;
    @Getter @Setter
    private String data;
    @Getter @Setter
    private String horario_saida;
    @Getter @Setter
    private String horario_chegada;
    @Getter @Setter
    private Double preco;

    public UberairDTO(String[] planes) {
        this.numero_voo = planes[0];
        this.aeroporto_origem = planes[1];
        this.aeroporto_destino = planes[2];
        this.data = planes[3];
        this.horario_saida = planes[4];
        this.horario_chegada = planes[5];
        this.preco = Double.valueOf(planes[6]);
    }

    @Override
    public String toString() {
        /*
         * Alteração de metodo toString()
         *
         * Diogo Tarnawisky
         *
         */

        StringBuffer sb = new StringBuffer();
        sb.append(getClass().getSimpleName().toUpperCase());
        sb.append(" [ ");
        Class<?> classe = this.getClass();

        Field fields[] = classe.getDeclaredFields();

        int count = 0;

        for (Field field : fields ) {
            count++;

            Object obj;

            try {

                obj = field.get(this);
                if (obj instanceof String) {
                    sb.append(field.getName());
                    sb.append(" = ");
                    sb.append(obj.toString());

                    if(count<fields.length)
                        sb.append(", ");

                }

            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }

        }


        sb.append(" ] ");
        return sb.toString();
    }

}

package com.linhas.areas.entity;

import lombok.Getter;
import lombok.Setter;

import java.lang.reflect.Field;
import java.time.LocalDate;



public class RegrasDeNegocio {

    @Getter @Setter
	private String origem;
    @Getter @Setter
	private String destino;
    @Getter @Setter
	private LocalDate data;

	public RegrasDeNegocio() {

	}


	public RegrasDeNegocio(String origem, String destino, LocalDate data) {
		this.origem = origem;
		this.destino = destino;
		this.data = data;
	}

	@Override
	public String toString() {
		/*
		 * Alteração de metodo toString()
		 *
		 * Diogo Tarnawisky
		 *
		 */

		StringBuffer sb = new StringBuffer();
		sb.append(getClass().getSimpleName().toUpperCase());
		sb.append(" [ ");
		Class<?> classe = this.getClass();

		Field fields[] = classe.getDeclaredFields();

		int count = 0;

		for (Field field : fields ) {
			count++;

			Object obj;

			try {

				obj = field.get(this);
				if (obj instanceof String) {
					sb.append(field.getName());
					sb.append(" = ");
					sb.append(obj.toString());

					if(count<fields.length)
						sb.append(", ");

				}

				if (obj instanceof LocalDate) {
					sb.append(field.getName());
					sb.append(" = ");
					sb.append(String.valueOf(obj).toString());

					if(count<fields.length)
						sb.append(", ");

				}


			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}

		}


		sb.append(" ] ");
		return sb.toString();
	}

}

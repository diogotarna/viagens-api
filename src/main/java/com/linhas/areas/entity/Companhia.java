package com.linhas.areas.entity;

import com.linhas.areas.dto.AeroportosDTO;
import lombok.Getter;
import lombok.Setter;

import java.lang.reflect.Field;

public class Companhia {

    @Getter @Setter
    private String nome;
    @Getter @Setter
    private String aeroporto;
    @Getter @Setter
    private String cidade;

    public Companhia() {

    }

    public Companhia(AeroportosDTO dto) {
       this.nome = dto.getNome();
       this.aeroporto = dto.getAeroporto();
       this.cidade = dto.getCidade();
    }


    @Override
    public String toString() {
        /*
         * Alteração de metodo toString()
         *
         * Diogo Tarnawisky
         *
         */

        StringBuffer sb = new StringBuffer();
        sb.append(getClass().getSimpleName().toUpperCase());
        sb.append(" [ ");
        Class<?> classe = this.getClass();

        Field fields[] = classe.getDeclaredFields();

        int count = 0;

        for (Field field : fields ) {
            count++;

            Object obj;

            try {

                obj = field.get(this);
                if (obj instanceof String) {
                    sb.append(field.getName());
                    sb.append(" = ");
                    sb.append(obj.toString());

                    if(count<fields.length)
                        sb.append(", ");

                }

            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }

        }


        sb.append(" ] ");
        return sb.toString();
    }
}

package com.linhas.areas.entity;

import com.linhas.areas.rules.FuncaoUtils;
import com.linhas.areas.dto.PlanesDTO;
import com.linhas.areas.dto.UberairDTO;
import lombok.Getter;
import lombok.Setter;

import java.lang.reflect.Field;
import java.time.LocalDate;
import java.time.LocalDateTime;


public class LinhasAerea implements Comparable<LinhasAerea>  {

    @Getter @Setter
    private String origem;
    @Getter @Setter
    private String destino;
    @Getter @Setter
    private LocalDate data;
    @Getter @Setter
    private LocalDateTime saida;
    @Getter @Setter
    private LocalDateTime chegada;
    @Getter @Setter
    private String operadora;
    @Getter @Setter
    private Double preco;

    public LinhasAerea() {

    }

    public LinhasAerea(String origem, String destino, LocalDate data, String operadora, LocalDateTime saida, LocalDateTime chegada,
               Double preco) {
        this.origem = origem;
        this.destino = destino;
        this.data = data;
        this.operadora = operadora;
        this.saida = saida;
        this.chegada = chegada;
        this.preco = preco;
    }

    public LinhasAerea(PlanesDTO dto) {
        this.operadora = "99Planes";
        this.origem = dto.getOrigem();
        this.destino = dto.getDestino();
        this.data = FuncaoUtils.parseData(dto.getData_saida());
        this.saida = FuncaoUtils.StrToTLocalDateTime(dto.getData_saida(), dto.getSaida());
        this.chegada = FuncaoUtils.StrToTLocalDateTime(dto.getData_saida(), dto.getChegada());
        this.preco = Double.valueOf(dto.getValor());
    }

    public LinhasAerea(UberairDTO dto) {
        this.operadora = "UberAir";
        this.origem = dto.getAeroporto_origem();
        this.destino = dto.getAeroporto_destino();
        this.data = FuncaoUtils.parseData(dto.getData());
        this.saida = FuncaoUtils.StrToTLocalDateTime(dto.getData(),dto.getHorario_saida());
        this.chegada = FuncaoUtils.StrToTLocalDateTime(dto.getData(),dto.getHorario_chegada());
        this.preco = Double.valueOf(dto.getPreco());
    }

    @Override
    public int compareTo(LinhasAerea linhasAerea) {
        LocalDate linha1 = this.saida.toLocalDate();
        LocalDate linha2 = linhasAerea.saida.toLocalDate();

        if (linha1.isBefore(linha2)) {  return -1;  } else if (linha1.isAfter(linha2)) {  return 1;  }

        return 0;
    }

    @Override
    public String toString() {
        /*
         * Alteração de metodo toString()
         *
         * Diogo Tarnawisky
         *
         */

        StringBuffer sb = new StringBuffer();
        sb.append(getClass().getSimpleName().toUpperCase());
        sb.append(" [ ");
        Class<?> classe = this.getClass();

        Field fields[] = classe.getDeclaredFields();

        int count = 0;

        for (Field field : fields ) {
            count++;

            Object obj;

            try {

                obj = field.get(this);
                if (obj instanceof String) {
                    sb.append(field.getName());
                    sb.append(" = ");
                    sb.append(obj.toString());

                    if(count<fields.length)
                        sb.append(", ");

                }

                if (obj instanceof Double) {
                    sb.append(field.getName());
                    sb.append(" = ");
                    sb.append(String.valueOf(obj).toString());

                    if(count<fields.length)
                        sb.append(", ");

                }


            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }

        }


        sb.append(" ] ");
        return sb.toString();
    }


}

Diogo Tarnawisky - 31/01/2020 

Repositório: https://bitbucket.org/diogotarna/viagens-api

Rodando a aplicação:

Abra a raiz do repositório, rode o comando "mvn clean install" , após a terminar de compilar, abrar a pasta target e execute o aereas-0.0.0.1 como o comando "java -jar aereas-0.0.0.1.jar"

Os endpoints são:

HTTP - GET http://localhost:8090/api/companhias
HTTP - POST http://localhost:8090/api/linhas/consultar

    body:json
    {
    	"origem": "BSB",
		"destino": "PLU",
		"data": "2019-02-10"
    }
    

